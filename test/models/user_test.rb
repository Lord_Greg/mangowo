require 'test_helper'

class UserTest < ActiveSupport::TestCase

  test "save correct user" do
    user = User.new email: "tester@example.com", password: "hasło123", username: "Tester"
    assert_equal true, user.save, "Nie dodano poprawnego użytkownika"
  end

  test "save blank user" do
    user = User.new
    assert_equal false, user.save, "Dodano pustego użytkownika"
  end

  test "save user without e-mail" do
    user = User.new password: "hasło123", username: "Tester"
    assert_equal false, user.save, "Dodano użytkownika bez e-maila"
  end

  test "save user without password" do
    user = User.new email: "tester@example.com", username: "Tester"
    assert_equal false, user.save, "Dodano użytkownika bez hasła"
  end

  test "save user without username" do
    user = User.new email: "tester@example.com", password: "hasło123"
    assert_equal false, user.save, "Dodano użytkownika bez nazwy użytkownika"
  end

  test "save user with wrong e-mail" do
    user = User.new email: "tester@example", password: "hasło123", username: "Tester"
    assert_equal false, user.save, "Dodano użytkownika z niepoprawnym e-mailem"
  end

  test "save user with password too short" do
    user = User.new email: "tester@example", password: "hasło", username: "Tester"
    assert_equal false, user.save, "Dodano użytkownika ze zbyt krótkim hasłem"
  end

end
