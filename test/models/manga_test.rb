require 'test_helper'

class MangaTest < ActiveSupport::TestCase

  test "save correct manga" do
    manga = Manga.new title_japanise: "Gakkou Gurashi!", publisher_id: 3
    assert_equal true, manga.save, "Nie dodano poprawnej mangi"
  end

  test "save blank manga" do
    manga = Manga.new
    assert_equal false, manga.save, "Dodano pustą mangę"
  end

  test "save manga without title" do
    manga = Manga.new publisher_id: 3
    assert_equal false, manga.save, "Dodano mangę bez tytułu"
  end

  test "save manga without publisher" do
    manga = Manga.new title_japanise: "Gakkou Gurashi!"
    assert_equal false, manga.save, "Dodano mangę bez wydawnictwa"
  end

end
