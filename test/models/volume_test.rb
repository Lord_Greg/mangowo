require 'test_helper'

class VolumeTest < ActiveSupport::TestCase

  test "save correct volume" do
    volume = Volume.new number: 999, manga_id: 1
    assert_equal true, volume.save, "Nie dodano poprawnego tomiku"
  end

  test "save blank volume" do
    volume = Volume.new
    assert_equal false, volume.save, "Dodano pusty tomik"
  end

  test "save volume without number" do
    volume = Volume.new manga_id: 1
    assert_equal false, volume.save, "Dodano tomik bez numeru"
  end

  test "save volume without manga" do
    volume = Volume.new number: 999
    assert_equal false, volume.save, "Dodano tomik bez mangi"
  end

  test "save two volumes with the same numbers for one manga" do
    volume_1 = Volume.new number: 999, manga_id: 1
    volume_2 = Volume.new number: 999, manga_id: 1
    volume_1.save
    assert_equal false, volume_2.save, "Dodano dwa tomiki o tym samym numerze dla jednej mangi"
  end

end
