require 'test_helper'

class PublisherTest < ActiveSupport::TestCase

  test "save correct publisher" do
    publisher = Publisher.new name: "Kotori"
    assert_equal true, publisher.save, "Nie dodano poprawnego wydawcy"
  end

  test "save publisher without name" do
    publisher = Publisher.new
    assert_equal false, publisher.save, "Dodano wydawcę bez nazwy"
  end

end
