# Mangowo #
Aplikacja webowa gromadząca oferty polskich wydawnictw mangowych. Po zarejestrowaniu się, pozwala na tworzenie własnej listy interesujących nas mang i ustawienie przypomnień e-mailowych w dniu premiery kolejnych tomików. System wyposażony jest też w opcję automatycznej aktualizacji premier dla 3 największych wydawnictw (JPF, Studio JG, Waneko).

Projekt wykonany w ramach pracy inżynierskiej - Luty 2017.