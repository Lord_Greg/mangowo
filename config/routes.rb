Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
  get '/', to: 'home#index', as: 'root'
  
  resources :publishers
  
  resources :mangas do
    resources :volumes, param: :number
  end


  get '/profiles/:id', to: 'profiles#show', as: 'profile'
  get '/profiles/:id', to: 'profiles#show', as: 'user'
  get '/profiles/:id/edit', to: 'profiles#edit', as: 'edit_profile'
  patch '/profiles/:id', to: 'profiles#update'
  delete '/profiles/:id', to: 'profiles#destroy'
  patch '/profiles/ban', to: 'profiles#ban'
  patch '/profiles/unban', to: 'profiles#unban'
  post '/profiles/ban', to: 'profiles#ban'
  post '/profiles/unban', to: 'profiles#unban'

  #routes for admins
  get '/admins', to: 'admins#index', as: 'admins'
  get '/admins/users_list', to: 'admins#users_list', as: 'users_list'
  

  #ajax routes
  post '/change_admin_state', to: 'ajax#change_admin_state'
  post '/change_notifications_state', to: 'ajax#change_notifications_state'
  post '/add_manga_to_list', to: 'ajax#add_manga_to_list'
  post '/remove_manga_from_list', to: 'ajax#remove_manga_from_list'
  post '/notification_read', to: 'ajax#notification_read'
  post '/notification_read_all', to: 'ajax#notification_read_all'
  post '/notification_delete', to: 'ajax#notification_delete'
  post '/notification_delete_read', to: 'ajax#notification_delete_read'
  
end
