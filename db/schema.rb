# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170126205250) do

  create_table "manga_lists", force: :cascade do |t|
    t.boolean "are_notifications_on", null: false
    t.integer "manga_id"
    t.integer "user_id"
    t.index ["manga_id"], name: "index_manga_lists_on_manga_id"
    t.index ["user_id"], name: "index_manga_lists_on_user_id"
  end

  create_table "mangas", force: :cascade do |t|
    t.string   "title_japanise",                     null: false
    t.string   "title_polish"
    t.boolean  "is_auto_actualized", default: false, null: false
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "release_cycle_id"
    t.integer  "publisher_id"
    t.float    "price"
    t.integer  "volume_count"
    t.index ["publisher_id"], name: "index_mangas_on_publisher_id"
    t.index ["release_cycle_id"], name: "index_mangas_on_release_cycle_id"
  end

  create_table "notifications", force: :cascade do |t|
    t.string   "body",       null: false
    t.boolean  "is_read",    null: false
    t.datetime "created_at", null: false
    t.integer  "user_id"
    t.index ["user_id"], name: "index_notifications_on_user_id"
  end

  create_table "publishers", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "release_cycles", force: :cascade do |t|
    t.integer "number_of_units", null: false
    t.integer "time_unit_id"
    t.index ["time_unit_id"], name: "index_release_cycles_on_time_unit_id"
  end

  create_table "time_units", force: :cascade do |t|
    t.string  "name",      null: false
    t.integer "day_count", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.date     "end_of_ban"
    t.boolean  "is_admin",               default: false, null: false
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.string   "username"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "volumes", force: :cascade do |t|
    t.integer  "number",                             null: false
    t.date     "release_date"
    t.boolean  "is_auto_actualized", default: false, null: false
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "manga_id"
    t.boolean  "is_date_confirmed"
    t.index ["manga_id", "number"], name: "index_volumes_on_manga_id_and_number", unique: true
    t.index ["manga_id"], name: "index_volumes_on_manga_id"
  end

end
