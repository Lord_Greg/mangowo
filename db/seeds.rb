# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

timeUnits = TimeUnit.create([
  { name: "Dzień", day_count: 1 },
  { name: "Tydzień", day_count: 7 },
  { name: "Miesiąc", day_count: 30 },
  { name: "Kwartał", day_count: 92 },
  { name: "Rok", day_count: 365 }
])

publishers = Publisher.create([
  { name: "JPF" },
  { name: "Studio JG" },
  { name: "Waneko" }
])

admin = User.create({
  email: "stepniewski.greg@gmail.com",
  username: "Greg",
  password: "admin123",
  is_admin: true,
  confirmed_at: DateTime.now
})
