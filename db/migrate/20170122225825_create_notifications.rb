class CreateNotifications < ActiveRecord::Migration[5.0]
  def change
    create_table :notifications do |t|
      t.string :body, null: false
      t.boolean :is_read, null: false
      t.datetime :created_at, null: false
    end
  end
end
