class RemovePriceFromVolumes < ActiveRecord::Migration[5.0]
  def change
    remove_column :volumes, :price, :float
  end
end
