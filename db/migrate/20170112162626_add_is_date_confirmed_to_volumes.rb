class AddIsDateConfirmedToVolumes < ActiveRecord::Migration[5.0]
  def change
    add_column :volumes, :is_date_confirmed, :boolean
  end
end
