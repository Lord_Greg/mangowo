class CreateTimeUnits < ActiveRecord::Migration[5.0]
  def change
    create_table :time_units do |t|
      t.string :name, unique: true, null: false
      t.integer :day_count, null: false
    end
  end
end
