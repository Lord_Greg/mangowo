class AddMangaRefToMangaList < ActiveRecord::Migration[5.0]
  def change
    add_reference :manga_lists, :manga, foreign_key: true
  end
end
