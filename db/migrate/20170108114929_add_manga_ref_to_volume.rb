class AddMangaRefToVolume < ActiveRecord::Migration[5.0]
  def change
    add_reference :volumes, :manga, foreign_key: true
  end
end
