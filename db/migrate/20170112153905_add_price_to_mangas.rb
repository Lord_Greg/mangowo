class AddPriceToMangas < ActiveRecord::Migration[5.0]
  def change
    add_column :mangas, :price, :float
  end
end
