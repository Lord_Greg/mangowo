class CreateReleaseCycles < ActiveRecord::Migration[5.0]
  def change
    create_table :release_cycles do |t|
      t.integer :number_of_units, null: false
    end
  end
end
