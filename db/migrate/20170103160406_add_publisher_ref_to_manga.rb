class AddPublisherRefToManga < ActiveRecord::Migration[5.0]
  def change
    add_reference :mangas, :publisher, foreign_key: true
  end
end
