class AddUniqueToVolume < ActiveRecord::Migration[5.0]
  def change
    add_index :volumes, [:manga_id, :number], :unique => true
  end
end
