class AddUserRefToMangaList < ActiveRecord::Migration[5.0]
  def change
    add_reference :manga_lists, :user, foreign_key: true
  end
end
