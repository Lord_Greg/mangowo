class AddTimeUnitRefToReleaseCycles < ActiveRecord::Migration[5.0]
  def change
    add_reference :release_cycles, :time_unit, foreign_key: true
  end
end
