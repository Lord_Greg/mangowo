class CreateMangaLists < ActiveRecord::Migration[5.0]
  def change
    create_table :manga_lists do |t|
      t.boolean :are_notifications_on, null: false
    end
  end
end
