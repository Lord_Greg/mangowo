class AddVolumeCountToMangas < ActiveRecord::Migration[5.0]
  def change
    add_column :mangas, :volume_count, :integer
  end
end
