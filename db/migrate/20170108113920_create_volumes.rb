class CreateVolumes < ActiveRecord::Migration[5.0]
  def change
    create_table :volumes do |t|
      t.integer :number, null: false
      t.date :release_date
      t.float :price
      t.boolean :is_auto_actualized, null:false
      t.timestamps
    end
  end
end
