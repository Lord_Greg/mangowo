class AddReleaseCycleRefToMangas < ActiveRecord::Migration[5.0]
  def change
    add_reference :mangas, :release_cycle, foreign_key: true
  end
end
