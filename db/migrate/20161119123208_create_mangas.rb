class CreateMangas < ActiveRecord::Migration[5.0]
  def change
    create_table :mangas do |t|
      t.string :title_japanise, null: false, unique: true
      t.string :title_polish
      t.boolean :is_auto_actualized, null: false
      t.timestamps
    end
  end
end
