class AddCustomFieldsToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :end_of_ban, :date
    add_column :users, :is_admin, :boolean, null: false, default: false
  end
end
