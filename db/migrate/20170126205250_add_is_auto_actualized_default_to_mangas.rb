class AddIsAutoActualizedDefaultToMangas < ActiveRecord::Migration[5.0]
  def change
    change_column :mangas, :is_auto_actualized, :boolean, :default => false
  end
end
