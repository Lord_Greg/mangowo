class AddIsAutoActualizedDefaultToVolumes < ActiveRecord::Migration[5.0]
  def change
    change_column :volumes, :is_auto_actualized, :boolean, :default => false
  end
end
