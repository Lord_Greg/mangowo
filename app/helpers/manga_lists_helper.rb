module MangaListsHelper

  def myProfile?()
    user_signed_in? && current_user.id.to_s == params[:id].to_s
  end

end
