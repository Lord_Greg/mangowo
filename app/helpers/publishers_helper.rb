module PublishersHelper
  def show_manga_title(manga)
    manga.title_polish +
    if manga.title_polish.present? && manga.title_polish != manga.title_japanise
      " (" + manga.title_japanise + ")"
    else
      ""
    end
  end
end
