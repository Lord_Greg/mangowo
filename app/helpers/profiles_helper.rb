module ProfilesHelper

  def my_profile?
    user_signed_in? && current_user.id == @user.id
  end
  
end
