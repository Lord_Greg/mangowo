module ApplicationHelper
  
  def admin?
    user_signed_in? && current_user.is_admin
  end

  def date_format_normal(date)
    date.strftime "%d-%m-%Y"
  end
  
end
