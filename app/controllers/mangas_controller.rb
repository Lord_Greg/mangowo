class MangasController < ApplicationController

before_action :check_admin, only: [:new, :create, :edit, :update, :destroy]
before_action :set_manga, except: [:index, :new, :create]

  def index
    @mangas = Manga.all
  end

  def new
    @manga = Manga.new
    @manga.release_cycle = ReleaseCycle.new
  end

  def create
    @manga = Manga.new(check_params)
    
    @manga.title_polish = @manga.title_japanise if @manga.title_polish.blank?
    @manga.validate_release_cycle!

    if @manga.save
    generate_volumes if params[:generate_volumes].present?

      redirect_to manga_path(@manga)
    else
      render "new"
    end
  end

  def show
  end

  def edit
    @manga.release_cycle = ReleaseCycle.new if @manga.release_cycle.blank?
  end

  def update
    updated_manga = Manga.new(check_params)
    updated_manga.title_polish = updated_manga.title_japanise if updated_manga.title_polish.blank?

    updated_manga.validate_release_cycle!

    if @manga.update(updated_manga.attributes.except("id", "created_at", "updated_at"))
      redirect_to manga_path(@manga)
    else
      render "edit"
    end
  end

  def destroy
    @manga.destroy
    redirect_to mangas_path
  end



  private def check_params()
    params.require(:manga).permit(:id, :title_japanise, :title_polish, :volume_count, :price, :publisher_id, :is_auto_actualized, :is_date_confirmed, release_cycle_attributes: [:number_of_units, :time_unit_id])
  end

  private def set_manga()
    @manga = Manga.find(params[:id])
  end


  private def generate_volumes()
    release_date = params[:start_volume_date]

    (params[:start_volume].to_i..params[:end_volume].to_i).each do |i|
      volume = @manga.volumes.new(
        number: i,
        release_date: release_date,
        is_date_confirmed: i == params[:start_volume].to_i ? true : false,
        is_auto_actualized: @manga.is_auto_actualized
      )
      volume.save

      release_date = volume.expected_date_of_next
    end

    
  end

end
