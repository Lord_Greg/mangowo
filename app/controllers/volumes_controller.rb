class VolumesController < ApplicationController

  before_action :check_admin, only: [:new, :create, :edit, :update, :destroy]
  before_action :set_manga, only: [:index, :new, :create]
  before_action :set_volume, except: [:index, :new, :create]

  def index
    redirect_to manga_path(@manga)
  end

  def new
    @volume = @manga.volumes.new
  end

  def create
    @volume = @manga.volumes.new(check_params)

    if @volume.save
      redirect_to manga_volume_path(@volume.manga, @volume)
    else
      render "new"
    end
  end

  def show
  end

  def edit
  end

  def update
    if @volume.update(check_params)
      redirect_to manga_volume_path(@volume.manga, @volume)
    else
      render "edit"
    end
  end

  def destroy
    @volume.destroy
    redirect_to manga_volumes_path
  end



  private def check_params()
    params.require(:volume).permit(:id, :number, :release_date, :is_date_confirmed, :is_auto_actualized)
  end

  private def set_manga()
    @manga = Manga.find(params[:manga_id])
  end

  private def set_volume()
    @volume = Volume.find_by(manga_id: params[:manga_id], number: params[:number])

    if @volume.blank?
      redirect_to manga_volumes_path
    end
  end

end
