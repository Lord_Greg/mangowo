class ProfilesController < ApplicationController

before_action :set_user, except: [:ban, :unban]
before_action :check_user, only: :show
before_action :check_admin, only: [:edit, :update, :ban, :unban, :destroy]

  def show
  end

  def edit
  end

  def update
    if @user.update_soft(check_params)
      redirect_to profile_path(@user)
    else
      render "edit"
    end
  end

  def destroy
    @user.destroy
    redirect_to users_list_path
  end


  def ban
    begin
      user = User.find params[:user_id]
      user.update_soft(end_of_ban: params[:date])
    end

    redirect_to users_list_path
  end

  def unban
    begin
      user = User.find params[:user_id]
      user.update_soft(end_of_ban: nil)
    end

    redirect_to users_list_path
  end



  private
  def set_user()
    begin
      @user = User.find(params[:id])
    rescue
      redirect_to root_path
    end
  end

  def check_params()
    params.require(:user).permit(:username)
  end

end
