class PublishersController < ApplicationController

before_action :check_admin, only: [:new, :create, :edit, :update, :destroy]
before_action :set_publisher, except: [:index, :new, :create]

  def index
    @publishers = Publisher.all
  end

  def new
    @publisher = Publisher.new
  end

  def create
    @publisher = Publisher.new(check_params)

    if @publisher.save
      redirect_to publisher_path(@publisher)
    else
      render "new"
    end
  end

  def show
  end

  def edit
  end

  def update
    if @publisher.update(check_params)
      redirect_to publisher_path(@publisher)
    else
      render "edit"
    end
  end

  def destroy
    @publisher.destroy
    redirect_to publishers_path
  end



  private def check_params()
    params.require(:publisher).permit(:id, :name)
  end

  private def set_publisher()
    @publisher = Publisher.find(params[:id])
  end

end
