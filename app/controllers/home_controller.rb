class HomeController < ApplicationController

  def index
    @volumes = Volume.soon.order(release_date: :asc)
  end

end
