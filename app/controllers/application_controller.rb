class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_filter :configure_permitted_parameters, if: :devise_controller?
  before_filter :banned?


  def banned?
    if current_user.present? && current_user.end_of_ban.present?
      end_of_ban = current_user.end_of_ban
      sign_out current_user
      redirect_to root_path, alert: "Twoje konto zostało zbanowane do #{end_of_ban}."
    end
  end


  def check_admin(path = root_path)
    if !user_signed_in? || !current_user.is_admin
      redirect_to path
    end
  end

  def check_user(path = new_user_session_path)
    unless user_signed_in?
      redirect_to path
    end
  end


  protected
  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:username])
    devise_parameter_sanitizer.permit(:account_update, keys: [:username])
  end

end
