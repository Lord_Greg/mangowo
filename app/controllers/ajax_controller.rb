class AjaxController < ApplicationController

  #admin
  def change_admin_state()
    respond_with_result do
      user = User.find params[:user_id]
      user.update_soft(is_admin: !user.is_admin)
    end
  end


  #manga list
  def change_notifications_state()
    respond_with_result do
      position = User.find(params[:user_id]).position_on_list(params[:manga_id])
      position.update(are_notifications_on: !position.are_notifications_on)
    end
  end

  def add_manga_to_list()
    respond_with_result do
      user = User.find params[:user_id]
      user.manga_lists.create(manga_id: params[:manga_id], are_notifications_on: true)
    end
  end

  def remove_manga_from_list()
    respond_with_result do
      user = User.find params[:user_id]
      user.manga_lists.where(manga_id: params[:manga_id]).first.destroy
    end
  end


  #notifications
  def notification_read()
    respond_with_result do
      notification = Notification.find params[:notification_id]
      notification.update(is_read: true)
    end
  end

  def notification_read_all()
    respond_with_result do
      user = User.find(params[:user_id])
      user.notifications.notRead.update_all(is_read: true)
    end
  end

  def notification_delete()
    respond_with_result do
      notification = Notification.find params[:notification_id]
      notification.destroy()
    end
  end

  def notification_delete_read()
    respond_with_result do
      user = User.find(params[:user_id])
      user.notifications.read.destroy_all()
    end
  end

  private
  def respond_with_result()
    respond_to do |format|
      success =
      begin
        yield
      rescue
        false
      end

      format.json do
        render json: { success: success }
      end
    end
  end

end
