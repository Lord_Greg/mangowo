class AdminsController < ApplicationController

before_action :check_admin

  def index
  end

  def users_list
    @users = User.without(current_user.id).order(created_at: :desc)
  end

end
