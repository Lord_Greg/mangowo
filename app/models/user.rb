class User < ApplicationRecord
  has_many :manga_lists
  has_many :notifications
  has_many :mangas, through: :manga_lists

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable

  scope :without, ->(id) { where.not(id: id) }
  scope :ban_ends_today, -> { where(end_of_ban: Date.today) }

  validates :email, presence: true, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create }
  validates :password, presence: true
  validates :username, presence: true

  def position_on_list(manga_id)
    begin
      self.manga_lists.where(manga_id: manga_id).first
    rescue
      nil
    end
  end


  def update_soft(attributes)
    backup = []
    success = false

    begin
      attributes.map do |key, value|
        backup.push [key => self[key]]
        self.update_attribute key, value
      end

      success = true
    rescue
      success = false

      backup.map do |key, value|
        self.update_attribute key, value
      end
      
    ensure
      return success
    end
  end

  def got_manga_on_list?(manga_id)
    self.mangas.find_by(id: manga_id).present?
  end
  
end
