class TimeUnit < ApplicationRecord
  has_many :release_cycles

  def self.months_names()
    [
      "", "Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec",
      "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"
    ]
  end

  def self.days_names_short()
    [ "Nd", "Pn", "Wt", "Śr", "Cz", "Pt", "Sb"]
  end

end
