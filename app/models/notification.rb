class Notification < ApplicationRecord
  belongs_to :user

  scope :read, -> { where(is_read: true) }
  scope :notRead, -> { where(is_read: false) }
  scope :toDisplay, -> { order(is_read: :asc).order(created_at: :desc) }

end
