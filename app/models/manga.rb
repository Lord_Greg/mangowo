class Manga < ApplicationRecord
  belongs_to :release_cycle, required: false
  belongs_to :publisher
  
  has_many :volumes, :dependent => :delete_all
  has_many :manga_lists
  has_many :users, through: :manga_lists

  accepts_nested_attributes_for :release_cycle

  validates :title_japanise, presence: true
  validates :publisher_id, presence: true
  
  def validate_release_cycle!()
    if self.release_cycle.present? && self.release_cycle.correct?
      existingCycle = self.release_cycle.findByAll.first
      
      if existingCycle.blank?
        self.release_cycle.save
        existingCycle = self.release_cycle.findByAll.first
      end
      
      self.release_cycle = existingCycle
    else
      self.release_cycle = nil
    end
  end


  def title_both()
    self.title_polish + (
      self.title_japanise != self.title_polish ? " (" + self.title_japanise + ")" : ""
    )
  end

end
