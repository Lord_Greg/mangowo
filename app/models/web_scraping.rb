require 'open-uri'

class WebScraping

##############
#Volume Count#
##############
  def get_manga_search(title)
    prefix = "https://www.mangaupdates.com/releases.html?search="
    suffix = "&act=archive&orderby=date&asc=asc"
    page = nokogiri_open prefix + transform_title(title) + suffix
    
    search_results = []

    if page.present?
      page.encoding = 'UTF-8'
      page.xpath('//*[@id="main_content"]/table/tr[3]/td/table/tr[1]/td/table/tr').each do |row|

        if row.xpath('./td[2]/a').first.present?
          manga = MangaupdatesResult.new row.xpath('./td[2]/a').first.content, row.xpath('./td[2]/a/@href').first.value
          
          unless search_results.any? { |element| element.url == manga.url }
            search_results.push manga
          end
        end
      end
    else
      search_results = nil
    end

    search_results
  end

  def get_volume_count(url)
    page = nokogiri_open url
    
    if page.present?
      page.encoding = 'UTF-8'
      page.xpath('//*[@id="main_content"]/table[2]/tr/td/div[1]/div[3]/div/div[14]').first.content.to_i
    else
      nil
    end
  end


####################
#Studio JG's Mangas#
####################

  def get_studio_jg_calendar()
    page = nokogiri_open "http://studiojg.pl/tytuly.html"

    if page.present?
      page.encoding = 'UTF-8'
      release_calendar = []

      page.xpath('//*[@id="left"]/table/tr').each do |row|
        if row.xpath('./td[2]/div/i').present? && row.xpath('./td[2]/div/i').first.content.to_s == "Nowości"
          volume_no = 0

          row.xpath('./td[2]/a').each do |link|
            if link.xpath('./img[@style="border: 1px solid red; "]').present?
              volume_no = extract_number(link.xpath('./@href').first.value.to_s.split("?nr=").last)
              break
            end
          end

          manga = MangaRelease.new(
            row.xpath('./td[2]/a[1]/b').first.content.to_s,
            volume_no,
            transform_date(row.xpath('./td[2]/text()[2]').first.content.to_s.split("Premiera: ").last)
          )
          
          if manga.ok?
            release_calendar.push manga
          end
        end
      end

      release_calendar
    else
      nil
    end
  end



##############
#JPF's Mangas#
##############

  def get_jpf_calendar()
    page = nokogiri_open "http://www.jpf.com.pl/index.php?content=cont_plan.php"

    if page.present?
      page.encoding = 'UTF-8'
      release_calendar = []

      page.xpath('//*[@id="content"]/div/div/table').each do |table|
        table.xpath('./tr').each do |row|
          if row.xpath('./td[@bgcolor="#00E18D"]').blank? #if not table header
            manga = MangaRelease.new(
              row.xpath('./td[2]').first.content.split(" #").first.to_s,
              extract_number(row.xpath('./td[2]').first.content.split(" #").last.to_s),
              transform_date(row.xpath('./td[1]').first.content.to_s)
            )
            
            if manga.ok?
              release_calendar.push manga
            end
          end
        end
      end

      release_calendar
    else
      nil
    end
  end



#################
#Waneko's Mangas#
#################

  def get_waneko_calendar()
    page = nokogiri_open "http://waneko.pl/zapowiedzi/"

    if page.present?
      page.encoding = 'UTF-8'
      date = Date.today
      release_calendar = []

      page.xpath('//div/table[@class="table-listing"]/tr').each do |row|

        if row['class'].present?
          row_content = row.xpath('./td').first.content.to_s
          year = extract_number(row_content)
          month = get_month(row_content)
          
          date = date.change(year: year, day: 1) if year.present?
          date = date.change(month: month, day: 1) if month.present?
        else
          manga = MangaRelease.new(
            row.xpath('./td[1]/a').first.content.split(": tom ").first.to_s,
            row.xpath('./td[1]/a').first.content.split(": tom ").last.to_i,
            date = date.change(day: extract_number(row.xpath('./td[2]').first.content.to_s))
          )
          
          if manga.ok?
            release_calendar.push manga
          end
        end
      end

      release_calendar
    else
      nil
    end
  end



  def nokogiri_open(page_url)
    try { Nokogiri::HTML open page_url }
  end


  def try(retries = 10, sleep_interval = 0.5)
    begin
      begin
        var = yield
        error = false
      rescue
        if retries <= 0
          return nil
        end

        error = true
        sleep sleep_interval
      end

      retries -= 1
    end while error

    var
  end


  private
  def transform_title(title)
    title.tr(" ", "+")
  end

  def extract_number(string)
    string.present? ? string.scan(/(\d+)/).flatten.first.to_i : nil
  end

  def get_month(string)
    case string.gsub(/\s.+/, '').downcase
    when "styczeń"
      1
    when "luty"
      2
    when "marzec"
      3
    when "kwiecień"
      4
    when "maj"
      5
    when "czerwiec"
      6
    when "lipiec"
      7
    when "sierpień"
      8
    when "wrzesień"
      9
    when "październik"
      10
    when "listopad"
      11
    when "grudzień"
      12
    else
      nil
    end
  end

  def transform_date(string)
    if string.present?
      date_parts = string.split("-").map { |s| s.to_i }

      begin
        Date.new(date_parts[0].to_i, date_parts[1].to_i, date_parts[2].to_i)
      rescue
        nil
      end
    else
      nil
    end
  end

end



class MangaupdatesResult

attr_accessor :title, :url

  def initialize(title, url)
    @title = title
    @url = url
  end

end


class MangaRelease

attr_accessor :manga_title, :volume_no, :release_date

  def initialize(manga_title, volume_no, release_date)
    @manga_title = manga_title
    @volume_no = volume_no
    @release_date = release_date
  end

  def ok?()
    @manga_title.present? && @volume_no.present? && @release_date.present?
  end
  
end
  