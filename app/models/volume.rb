class Volume < ApplicationRecord
  belongs_to :manga

  validates :number, presence: true
  validates_uniqueness_of :number, :scope => [:manga_id]

  scope :releasedToday, -> { where(release_date: Date.today) }
  scope :releasedAt, ->(date) { where(release_date: date) }
  scope :soon, -> { where('release_date >= ?', Date.today).limit(5) }

  scope :toActualization, -> {
    joins(:manga)
      .where(mangas: { is_auto_actualized: true } )
      .where(is_auto_actualized: true)
      .where(release_date: 1.month.ago..1.month.from_now)
    }


  def title_full()
    self.manga.title_polish + " tom " + number.to_s
  end


  def title_extended()
    title = manga.title_polish
    title += "(" + manga.title_japanise + ")" if manga.title_polish != manga.title_japanise
    title += " tom " + number.to_s
  end


  def expected_date_of_next()
    if self.release_date.present? && self.manga.release_cycle.present?
      self.release_date + self.manga.release_cycle.days
    end
  end


  def to_param
    number.to_s
  end

end
