class ReleaseCycle < ApplicationRecord
  belongs_to :time_unit
  has_many :mangas

  scope :findByAll, ->(number_of_units, time_unit_id) {
    where(number_of_units: number_of_units, time_unit_id: time_unit_id)
  }


  def correct?
    self.number_of_units.present? && self.time_unit_id.present? ? true : false
  end

  def days()
    self.number_of_units * self.time_unit.day_count
  end


  def findByAll(number_of_units = self.number_of_units, time_unit_id = self.time_unit_id)
    ReleaseCycle.findByAll(number_of_units, time_unit_id)
  end
  
end
