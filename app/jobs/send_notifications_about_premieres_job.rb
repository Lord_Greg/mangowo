class SendNotificationsAboutPremieresJob < ApplicationJob
  queue_as :default

  def perform(*args)
    puts "Rozpoczęto proces rozsyłania powiadomień."
    
    notification_count = 0
    email_count = 0

    Volume.releasedToday.each do |volume|
      volume.manga.manga_lists.each do |position|
        #send notifications
        position.user.notifications.create(
          body: "Dziś premiera " + position.manga.title_polish + " #" + volume.number.to_s,
          is_read: false
        )
        notification_count += 1

        #send emails
        if position.are_notifications_on
          PremiereMailer.premiere_email(position.user, volume).deliver_later
          email_count += 1
        end

      end
    end

    puts "Zakończono proces rozsyłania powiadomień."
    puts "Powiadomień wysłanych: #{notification_count}."
    puts "E-maili do wysłania: #{email_count}."
  end

end
