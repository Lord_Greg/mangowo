class ReleaseDatesActualizationJob < ApplicationJob
  queue_as :default

  def perform(*args)
    puts "Rozpoczęto proces aktualizację tomików."

    actualized_count = 0
    scraper = WebScraping.new

    if (volumes = Volume.toActualization).present?
      publisher = "JPF"
      actualized_count += actualize_volumes(
        volumes,
        scraper.get_jpf_calendar,
        publisher
      )

      publisher = "Studio JG"
      actualized_count += actualize_volumes(
        volumes,
        scraper.get_studio_jg_calendar,
        publisher
      )

      publisher = "Waneko"
      actualized_count += actualize_volumes(
        volumes,
        scraper.get_waneko_calendar,
        publisher
      )
    end

    puts "Zakończono proces aktualizacji tomików.\nŁącznie tomów zaaktualizowanych: #{actualized_count.to_s}."
  end


  private
  def actualize_volumes(volumes, release_callendar, publisher_name)
    puts "Rozpoczęto aktualizację dla wydawnictwa #{publisher_name}."
    
    actualized_count = 0
    
    if volumes.present? && release_callendar.present?  
      volumes.select { |volume| volume.manga.publisher.name == publisher_name }.each do |volume|
        (release_callendar.select { |manga| manga.manga_title.downcase == volume.manga.title_polish.downcase })
          .each do |manga| 
            actualized_count += 1 if volume.number == manga.volume_no\
              && volume.update(release_date: manga.release_date, is_date_confirmed: true)
        end
      end
    end

    puts "Zaaktualizowano #{actualized_count.to_s} tomów wydawnictwa #{publisher_name}."
    actualized_count
  end

end
