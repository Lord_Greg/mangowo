class EndBansJob < ApplicationJob
  queue_as :default

  def perform(*args)
    puts "Rozpoczęto proces kończenia banów."

    unban_count = 0
    
    if (users = User.ban_ends_today).present?
      users.each do |user|
        unban_count += 1 if user.update_soft(end_of_ban: nil)
      end
    end

    puts "Zakończono proces kończenia banów.\nŁącznie użytkowników odblokowanych: #{unban_count.to_s}."
  end
end
