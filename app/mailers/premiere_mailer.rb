class PremiereMailer < ApplicationMailer
  include Rails.application.routes.url_helpers

  default from: 'projekt.mangowo@wp.pl'
 
  def premiere_email(user, volume)
    @user = user
    @volume = volume
    @volume_url = manga_volume_url volume.manga, volume
    @root_url = root_url
    mail(to: @user.email, subject: 'Jedna z twoich mang ma dziś premierę')
  end

end
